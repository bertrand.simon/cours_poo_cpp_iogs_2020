#+Title: Programmation orientée objet en C++ - TD6
#+subtitle: D'apès un TD originellement proposé par Brett Ridel et Thibaud Lambert
#+Author: Bertrand SIMON
#+Email: bertrand.simon@institutoptique.fr
#+DESCRIPTION:  Polymorphisme
#+KEYWORDS: 
#+LANGUAGE:  fr
#+LaTeX_CLASS: article
#+latex_class_options: [18pt]
#+LATEX_CLASS_OPTIONS: [pdftex, justified, a4paper]
#+OPTIONS: :toc nil
#+STARTUP: latexpreview
#+latex_header: \usepackage{setspace}
#+latex_header: \onehalfspacing
#+LATEX_HEADER: \usepackage{tabularx}
#+LATEX_HEADER: \usepackage[pdftex]{graphicx}
#+LATEX_HEADER: \usepackage{array}
#+LATEX_HEADER: \usepackage{geometry}         % Dï¿½finir les marges
#+LATEX_HEADER: \usepackage{srcltx}               
#+LATEX_HEADER: \usepackage{amsxtra}
#+LATEX_HEADER: \usepackage[]{amsmath}
#+LATEX_HEADER: \usepackage[]{amssymb}
#+LATEX_HEADER: \usepackage[]{subfigure}
#+LATEX_HEADER: \usepackage{multirow}
#+LATEX_HEADER: \usepackage{psfrag}
#+LATEX_HEADER: \usepackage{url}
#+LATEX_HEADER: \usepackage{circuitikz}
#+LATEX_HEADER: \usepackage{hyperref}
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usepackage{pgfplots}
#+LATEX_HEADER: \usepackage{pgfplots}
#+LATEX_HEADER: \usepackage{eso-pic}


* Polymorphisme de type
Durant les séances précédentes, vous avez implémentez une architecture logicielle centrée autour de
formes géométrique. Ainsi, nous avons deux classe =Cercle= et =Polygone=, héritant toutes deux de la
classe =FormeGeometrique=. De cette manière, toutes les méthodes définies et les attributs /protected/ d’une
=FormeGeometrique= sont accessibles pour les classes =Cercle= et =Polygone=.
Nous aimerions maintenant pouvoir stocker dans un =std::vector= des formes géométriques, quel que soit
le type réel (=FormeGeometrique=, =Polygone= ou =Cercle=). Pour ajouter un objet dans un =std::vector= le
constructeur par copie est utilisé. Si notre vecteur est de type =std::vector<FormeGeometrique>= alors le
constructeur par copie de =FormeGeometrique= sera utilisé et non celui de =Cercle= ou de =Polygone=. Ainsi
le type réel des objets contenus dans le vecteur ne pourra être que =FormeGeometrique=. Pour résoudre
cela, il faut stocker un pointeur : =std::vector<FormeGeometrique*>=. Ainsi le type réel de l’objet stocké
est conservé.
1. Déclarez dans le main un vecteur de pointeurs : =std::vector<FormeGeometrique*>= formes;
2. À l’aide de la méthode =push_back()= de =std::vector=, ajoutez une
   instance de =Cercle=, une de =Polygone= et une de
   =FormeGeometrique= au vecteur =formes=. Pour récupérer un pointeur
   sur objet, utilisez la syntaxe suivante : =&mon objet=.
3. À l’aide d’une boucle =for=, appelez sur chacun des éléments de
   formes la méthode =nom()= et affichez le résultat sur la sortie standard.
* La méthode périmètre
Si vous regardez attentivement le code existant, la classe =Polygone= possède une méthode =perimetre()=.
Nous aimerions maintenant que n’importe quelle forme géométrique puisse avoir ce même comportement.
1. Ajoutez une méthode =float FormeGeometrique::perimetre() const= qui renvoie =0= par défaut.
2. Dans le main, affichez le périmètre sur les éléments du vecteur
   formes. Affichez également le périmètre du polygone =poly=
   existant. La méthode =Polygone::perimetre()= appelée depuis le
   vecteur formes ne devrait pas renvoyer la même valeur que
   lorsqu’elle est appelle directement sur le polygone lui-même. En
   effet, le vecteur =formes= contient des =FormeGeometrique*=, et
   donc les méthodes appelées sont celles d’une
   =FormeGeometrique=. Pour utiliser la méthode
   =Polygone::perimetre()= plutôt que la méthode
   =FormeGeometrique::perimetre()= il est impératif de déclarer la
   méthode comme étant virtuelle. 
3. À l’aide du mot clé virtual, rendez la méthode
   =FormeGeometrique::perimetre() virtuelle= :  
#+BEGIN_SRC c++
 virtual float   FormeGeometrique::perimetre() const. 
#+END_SRC
  Vérifiez que l’appel de la méthode depuis le vecteur formes renvoie une valeur non nulle pour le polygone.
4. Implémentez la méthode =perimetre()= pour la classe =Cercle=.
5. Tel que vous l’avez implémenté, une =FormeGeometrique= renvoie un
   périmètre nul par défaut, car à ce stade là nous ne sommes pas
   capable de définir le comportement à adopter. Plutôt que de
   renvoyer une valeur nulle par défaut, qui n’a aucun sens
   géométrique, nous allons déclarer la méthode
   =FormeGeometrique::perimetre= comme étant virtuelle pure. Cela
   signifie que la méthode est virtuelle et n’est pas définie. Pour
   cela, modifiez la déclaration de la méthode :
#+BEGIN_SRC c++
 virtual float  FormeGeometrique::perimetre() const = 0. 
#+END_SRC
Notez les mots clés
   =virtual=  == 0= qui permettent de rendre la méthode virtuelle pure. 
Une classe possédant une méthode virtuelle pure ne peut être
   instanciée. Elle est appelée classe abstraite. Dans notre cas,
   =FormeGeometrique= est une classe abstraite.

* La méthode centre de gravité
Nous allons les même modifications que pour la méthode périmètre.
1. Ajoutez une méthode virtuelle pure =FormeGeometrique::centreDeGravite=.
2. Les classes filles héritant d’une classe abstraite doivent
   implémenter les méthodes virtuelles pures des classes parents sous
   peine d’être à leur tour une classe abstraite. Dans notre cas,
   =Cercle= n’implémente pas =centreDeGravite=, c’est donc une classe
   abstraite. Modifiez en conséquence votre implémentation pour que cela ne soit plus le cas.
3. Dans le main, affichez le centre
