#ifndef POINT_H
#define POINT_H


class Point
{
public:
    Point();
    Point(float x, float y);
    Point(Point const& P);

    void info();
    void translate(float vx, float vy);
    double distance(Point const& P) const;
    float getx() const;
    float gety() const;
    void setx(float x);
    void sety(float y);
    Point & operator =(Point const& other);
private:
    float _x;
    float _y;

};

#endif // POINT_H
