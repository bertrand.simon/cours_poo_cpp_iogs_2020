#ifndef CERCLE_H
#define CERCLE_H
#include "point.h"
#include "formegeometrique.h"

class Cercle:public FormeGeometrique
{
public:
    Cercle(Point c, float r);
    Cercle(Cercle const &c);
    void translate (float vx, float vy);
    void magnify(float k);
    float getx  ()const;
    float gety  ()const;
    Cercle & operator =(Cercle const& autre);
    void info();
    virtual float perimetre() const;
private:
    Point _c;
    float _r;
};

#endif // CERCLE_H
