#include <iostream>

#include "cercle.h"
#include "point.h"
#include "formegeometrique.h"
#include <math.h>

#define _USE_MATH_DEFINES

Cercle::Cercle(Point c, float r):FormeGeometrique("Cercle") {
    _c.setx(c.getx());
    _c.sety(c.gety());
    _r=r;
}
Cercle::Cercle(Cercle const& c){
    _c.setx(c.getx());
    _c.sety(c.gety());
    _r =c._r;
}

void Cercle::translate (float vx, float vy) {
    _c.translate(vx, vy);
}

void Cercle::magnify(float k){
    _r=k*_r;
}

float Cercle::getx() const{
    return _c.getx();
}

float  Cercle::gety() const{
    return _c.gety();
}
Cercle & Cercle::operator =(Cercle const& autre){
    _c=autre._c; // d'où l'interêt d'avoir codé la surcharge de l'opérateur "=" pour la coie.
    _r=autre._r;

}
void Cercle::info(){
    std::cout<< "Cercle de centre ";
    _c.info();
    std::cout<<" et de rayon: "<<_r<<std::endl;
}
float Cercle::perimetre() const{
    return _r*2*M_PI;
}
