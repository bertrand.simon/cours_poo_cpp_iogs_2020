#include <iostream>
#include <cmath>
#include <math.h>
#include "polygone.h"
#include "formegeometrique.h"
Polygone::Polygone(unsigned int nb_sommets):FormeGeometrique("polygone"), _sommets(nb_sommets)
{
    /* On  voit ici une autre façon d'initialiser _sommets, pas dans les accolades avec =, mais en en-tête...*/
    /* Le constructuer permet de tracer un polygone régulier de nb_sommets et de "rayon 1"*/
    for (unsigned int i=0; i<nb_sommets; ++i) {
        Point P(cos(i*2*M_PI/nb_sommets),sin(i*2*M_PI/nb_sommets));
                _sommets.push_back(P);
    }
}

Polygone::Polygone(const Polygone &poly):FormeGeometrique("polygone"), _sommets(poly._sommets){

}

Polygone::~Polygone()
{
}

Polygone &Polygone::operator=(const Polygone &other)
{
    if(this != &other) {
        /* ici this est un pointeur vers l'objet dans lequel nous voulons copier other.
       le test permet simplemet de s'assurer que l'on ne copie pas un objet à partir de lui-même*/
        FormeGeometrique::operator =(other);
        _sommets = other._sommets;
    }
    return *this;
}

void Polygone::initPoint(unsigned int indice, const Point &p)
{
    _sommets.at(indice) = p;
}

void Polygone::initPoint(unsigned int indice, float x, float y)
{
    _sommets.at(indice) = Point(x,y);
}

unsigned int Polygone::nbPoints() const
{
    return _sommets.size();
}

Point Polygone::centreDeGravite() const
{
    float x=0,y=0;
    for(Point const & p : _sommets) {
        x += p.getx();
        y += p.gety();
    }
    x /= _sommets.size();
    y /= _sommets.size();

    return Point(x,y);
}

float Polygone::perimetre() const
{
    unsigned int nb_sommet = _sommets.size();
    float perimetre = 0;
    for(unsigned int i=0; i<nb_sommet; i++)
        perimetre += _sommets[i].distance(_sommets[(i+1)%nb_sommet]);

    return perimetre;
}

void Polygone::info()
{
    std::cout << "nb sommets: " << _sommets.size() << std::endl;
    for(unsigned int i=0; i<_sommets.size(); i++) {
        std::cout << "s" << i << ": ";
        _sommets[i].info();
    }
    std::cout << std::endl;
}
