TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    point.cpp \
    cercle.cpp \
    polygone.cpp \
    formegeometrique.cpp

HEADERS += \
    point.h \
    cercle.h \
    polygone.h \
    formegeometrique.h
