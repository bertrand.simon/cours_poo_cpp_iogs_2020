#include <string>
#include <iostream>

#include "point.h"
#include "polygone.h"
#include "cercle.h"
#include "formegeometrique.h"

using namespace std;

int main(int argc, char *argv[])
{
    // Forme geometrique

    // Polygone
    Point p0(-2.5,3.7);
    Point p1(1.5,2.2);
    Point p2(2.8,3.9);
    Polygone poly(3);
    poly.initPoint(0,p0);
    poly.initPoint(1,p1);
    poly.initPoint(2,p2);
    std::cout << "poly : " << poly.nom() << std::endl;
    poly.info();

    // Cercle
    Cercle c0(Point(1.7,4.3),3.8);
    std::cout << "c0 : " << c0.nom() << std::endl;
    c0.info();
    std::vector<FormeGeometrique*> v;
    v.push_back(&poly);
    v.push_back(&c0);
    std::vector<FormeGeometrique*>::iterator it;
    for (it=v.begin(); it!=v.end(); ++it){
        std::cout<<"Le périmètre de la forme "<<(*it)->nom()<<" vaut : "<<(*it)->perimetre()<<std::endl;
    }
    return 0;

}
