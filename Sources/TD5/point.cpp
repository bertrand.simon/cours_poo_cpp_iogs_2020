#include <iostream>
#include <cmath>
#include "point.h"

Point::Point()
{
_x=0.0;
_y=0.0;
}

Point::Point (float x, float y) {
    _x=x;
    _y=y;
}

Point::Point (Point const& P) {
    _x=P._x;
    _y=P._y;
}

void Point::info(){
    std::cout<<"point de coordonnées x="<<_x<<", y="<<_y<<std::endl;
}

double Point::distance(Point const& P) const{
    return sqrt(_x*_x+_y*_y);
}

void Point::translate(float vx, float vy){
    _x=_x+vx;
    _y=_y+vy;
}

float Point::getx() const{
    return _x;
}

float Point::gety() const{
    return _y;
}

void Point::setx(float x){
    _x=x;
}

void Point::sety(float y){
    _y=y;
}
Point & Point::operator =(Point const& other) {
    _x=other.getx();
    _y=other.gety();
}
