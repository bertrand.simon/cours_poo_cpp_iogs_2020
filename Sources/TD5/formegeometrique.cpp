#include <string>

#include "formegeometrique.h"

FormeGeometrique::FormeGeometrique()
{
_nom="forme";
}
FormeGeometrique::FormeGeometrique(const std::string nom)
{
_nom= nom;
}
FormeGeometrique &FormeGeometrique::operator=(const FormeGeometrique &other)
{
    if(this != &other) {
        _nom = other._nom;
    }
    return *this;
}
std::string FormeGeometrique::nom(){
    return _nom;
}
