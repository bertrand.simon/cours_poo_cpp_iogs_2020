#ifndef POLYGONE_H
#define POLYGONE_H

#include <vector>

#include "point.h"
#include "formegeometrique.h"

class Polygone:public FormeGeometrique
{
public:

    Polygone(unsigned int nb_sommets); //constructeur par défaut prend nécessairement un nombre de sommets.
    Polygone(Polygone const & poly);// constructeur par copie.
    ~Polygone();

    Polygone & operator =(Polygone const & other);

    void initPoint(unsigned int indice, Point const &p);
    void initPoint(unsigned int indice, float x, float y);

    unsigned int nbPoints() const;
    Point centreDeGravite() const;

    void info();
    virtual float perimetre() const;

protected:
    std::vector<Point>_sommets;
};

#endif // POLYGONE_H
