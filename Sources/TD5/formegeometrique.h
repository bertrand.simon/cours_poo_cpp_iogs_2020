#ifndef FORMEGEOMETRIQUE_H
#define FORMEGEOMETRIQUE_H
#include <iostream>
#include<string>

class FormeGeometrique
{
public:
    FormeGeometrique();
    FormeGeometrique(std::string const nom);
    FormeGeometrique & operator=(FormeGeometrique const & other);
    std::string nom();
    virtual float perimetre() const =0;
private:
    std::string _nom;
};

#endif // FORMEGEOMETRIQUE_H
