#include <iostream>
namespace Stack {
  static const int max_size= 200;
  static char v[max_size];
  static int top=0;
  void push(char c){
    if (top<(max_size-1)) {
      v[top]=c;
      top++;
    }
    else std::cout<<"pile pleine"<<std::endl;
  }
  char pop(){
    if (top>=0) {
      return v[top];
      top--;
    }
    else std::cout<<"pile vide"<<std::endl;
  }
}
