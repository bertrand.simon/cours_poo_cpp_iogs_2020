#include <iostream> /* appel de la librairie de gestion des flux */
#include <cstdlib>  /* librairie du C standard */

using namespace std; /* utiliser directement 'cin' et 'cout'*/

int length( char* buf) { //passage par variable utilisant une référence
  int l=0;
  while (buf[l++]!='\0');
  return l;
}
char* inputstr(char* buf) {
  cin >> buf; /* copie entrée dans buffer*/
  int l=length(buf);
  char* var=NULL;
  var =(char* ) malloc(l * sizeof(char)); // rappel, buffer est un pointeur vers le premier élément de tableau
  for (int i=0; i<l;i++) var[i]=buf[i]; // copie du buffer dans nom (alloué dynamquement pour contenir exactement la chaine entrée
  return var;
}
int main() /*bloc principal*/ {
  char buffer[255]; /* déclare un tableau de char*/

  cout << "Quel est votre nom ? : " << endl; /* affiche la question*/
  char* nom=inputstr(buffer);
  cout << "Quel est votre prenom ? : " << endl; /* affiche la question*/
  char* prenom=inputstr(buffer);
  /* affiche bonjour, nom entré*/
  cout << "Quel est le nom de jeune fille de vore mère? : " << endl; /* affiche la question*/
 char* mere=inputstr(buffer);
 cout << "Quelle est votre ville de naissance ? : " << endl; /* affiche la question*/
 char* ville=inputstr(buffer);
 cout << "Quelle est la marque de votre voiture ? (ou votre marque préférée)" << endl; /* affiche la question*/
  char* car=inputstr(buffer);
  cout << "Quel est le dernier medicament que vous ayez pris ? : " << endl; /* affiche la question*/
  char* medic=inputstr(buffer);
  cout<<"bonjour, ";
  for (int i=0; i<3; i++) cout<<nom[i];
  for (int i=0; i<2; i++) cout<<prenom[i];
  cout <<" ";
  for (int i=0; i<2; i++) cout<<mere[i];
  for (int i=0; i<3; i++) cout<<ville[i];
  cout <<", ";
  for (int i=length(nom)-2; i>length(nom)-4; i--) cout<<nom[i];
  for (int i=0; i<3; i++) cout<<car[i];
  cout <<" of "<<medic<<endl;
  return 0;
}
