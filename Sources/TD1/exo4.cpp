#include <iostream> /* appel de la librairie de gestion des flux */
#include <cstdlib>  /* librairie du C standard */

using namespace std; /* utiliser directement 'cin' et 'cout'*/

int length( char* buf) { //passage par variable
  int l=0;
  while (buf[l++]!='\0');
  return l;
}

int permute(char* a, char* b){
  char c=*a;
  *a= *b;
  *b= c;
  return 0;
}

int inverse(char* a){
  int l=length(a);
  for(int i=0;i<l/2;i++){
    permute(&(a[i]),&(a[l-i-2]));
  }
}

bool isPalindrome(char* a){
  bool t=false;
  int l=length(a);
  int k=0;
  while((a[k]==a[l-2-k]) and (k<l/2)){
    k++;
    t=true;
    cout << k<< a[k]<<" and " << a[l-2-k]<<" :  "<<t<<endl;
    
  }
  return t;
}

char* inputstr(char* buf) {
  cin >> buf; /* copie entrée dans buffer*/
  int l=length(buf);
  char* var=NULL;
  var =(char* ) malloc(l * sizeof(char)); // rappel, buffer est un pointeur vers le premier élément de tableau
  for (int i=0; i<l;i++) var[i]=buf[i]; // copie du buffer dans nom (alloué dynamquement pour contenir exactement la chaine entrée
  return var;
}

int main() /*bloc principal*/ {
  char buffer[255]; /* déclare un tableau de char*/
  cout << "Entrez un mot " << endl; /* affiche la question*/
  char* mot=inputstr(buffer);
  inverse(mot);
  cout<<"retournement = "<<mot<<endl;
  inverse(mot);
  if (isPalindrome(mot)) cout<<mot<<" est un palindrome !"<<endl;
  else cout<<mot<<" n'est pas un palindrome"<<endl;
  return 0;
}
