#include <iostream> /* appel de la librairie de gestion des flux */
#include <cstdlib>  /* librairie du C standard */

using namespace std; /* utiliser directement 'cin' et 'cout'*/

int length( char* buf) { //passage par variable utilisant une référence
  int l=0;
  while (buf[l++]!='\0');
  return l;
}

int main() /*bloc principal*/ {
  char buffer[255]; /* déclare un tableau de char*/
  char* nom=NULL; // Création d'un pointeur de char pour alloc dynamique de nom
  cout << "Quel est votre nom ? : " << endl; /* affiche la question*/
  cin >> buffer; /* copie entrée dans buffer*/
  nom =(char* ) malloc(length(buffer) * sizeof(char)); // rappel, buffer est un pointeur vers le premier élément de tableau
  for (int i=0; i<length(buffer);i++) nom[i]=buffer[i]; // copie du buffer dans nom (alloué dynamqiuement pour contenir exactement la chaine entrée
  cout << "bonjour, " << nom << endl; /* affiche bonjour, nom entré*/
  cout <<"la longeur de la chaine est: "<<length(buffer)-1<<endl;
  return 0;
}
