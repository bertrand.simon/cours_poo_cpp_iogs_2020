#include <iostream>

#include "stack.h"

int main()
{
    FILE* fichier = NULL;
    int car = 0;

    fichier = fopen("/home/bs/Documents/Enseignement/POO_cpp/Cours/sources/TD/TD2b/test.txt", "r");
    Stack::stack pile=Stack::create();//creation de la pile
    bool ouvre=false;
    if (fichier != NULL)
    {
        // Boucle de lecture des caractères un à un
        do
        {
            car = fgetc(fichier); // On lit le caractère
            ouvre=(car=='(')||(car=='[')||(car=='{')||(car=='<');
            if (ouvre) Stack::push(pile,car);
            switch (car) {
            case ')':
                if (pile.v[pile.top]=='(') {
                    printf("%c",car);
                    Stack::pop(pile);
                }
                break;
            case ']':
                if (pile.v[pile.top]=='[') {
                    printf("%c",car);
                    Stack::pop(pile);
                }
                break;
            case '}':
                if (pile.v[pile.top]=='{') {
                    printf("%c",car);
                    Stack::pop(pile);
                }
                break;
            case '>':
                if (pile.v[pile.top]=='<') {
                    printf("%c",car);
                    Stack::pop(pile);
                }
                break;
            default:
                printf("%c", car);
                break;
            }
        } while (car != EOF); // On continue tant que fgetc n'a pas retourné EOF (fin de fichier)
        if (pile.top==-1) std::cout<<"Bravo ! votre texte est correctement parenthésé !"<<std::endl;
        else{
            std::cout<<"Argl! il y a une erreur de parenthèse, état de la pile :"<<std::endl<<std::endl;
            while (pile.top>-1){
                std::cout<<Stack::pop(pile)<<std::endl<<std::endl;
            }
        }
        Stack::destroy(pile);
        fclose(fichier);
    }
    return 0;
}
