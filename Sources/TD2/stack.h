#ifndef STACK_H
#define STACK_H

namespace Stack {
const int max=16;
const int max_size=200;
struct rep{
    int top;
    int stackId;
    char v[max_size];
};
typedef rep& stack;
static rep stacks[max];
static bool used[max];
stack create();
stack destroy(stack s);
void push(stack s, char c);
char pop(stack s);
};
#endif // STACK_H
