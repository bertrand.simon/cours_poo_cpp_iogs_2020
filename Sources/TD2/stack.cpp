#include <iostream>

#include "stack.h"

int m=Stack::max_size;
int M=Stack::max;

Stack::stack Stack::create(){
    int i=0;
    while((i++<M)&&(Stack::used[i]));
    if (i<M){
        struct Stack::rep& s=Stack::stacks[i];// référence à retourner
        s.stackId=i;
        s.top=-1;
        Stack::used[i]=true;
        std::cout<<"pile créée Id="<<s.stackId<<" top="<<s.top<<std::endl;
        return s;
    }
    else{
        std::cout<<"plus de pile disponible"<<std::endl;
    }
}

Stack::stack Stack::destroy(Stack::stack s){
    int id=s.stackId;
    Stack::used[id]=false;
}

void Stack::push(Stack::stack s,char c){
    int t=s.top;
    std::cout<<s.top<<std::endl;
    if (t<m) {
        s.v[++s.top]=c;
    }
    else
    {
        std::cout<<"La pile id="<<s.stackId<<" est pleine !"<<std::endl;
    }
}
char Stack::pop(Stack::stack s){
    if (s.top>-1) {
        return s.v[s.top--];
    }
    else {
        std::cout<<"La pile id="<<s.stackId<<" est vide !"<<std::endl;
    }
}
