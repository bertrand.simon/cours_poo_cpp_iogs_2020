TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    stack.cpp

HEADERS += \
    stack.h

DISTFILES += \
    test.txt
