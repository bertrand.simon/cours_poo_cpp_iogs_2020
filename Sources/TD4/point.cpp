#include <iostream>
#include <math.h>
#include "point.h"

Point::Point()
{
    _x=0.0;
    _y=0.0;
}

Point::Point(float x, float y){
    _x=x;
    _y=y;
}

Point::Point(Point const &P){
    _x=P.getx();
    _y=P.gety();
}

Point& Point::operator= (Point const &P){
    Point res(P.getx(),P.gety());
    return res;
}

float Point::getx() const{
    return _x;
}

float Point::gety() const {
    return _y;
}

void Point::info() const{
    std::cout<<"point de coordonnées: "<<_x<<", "<<_y<<std::endl;
    return 0;
}

void Point::translate(float vx, float vy){
    _x+=vx;
    _y=vy;
    return 0;
}

float Point::distance(Point const &P){
    float d=sqrt(pow(_x-P.getx(),2)+pow(_y-P.gety(),2));
    return d;
}
