#include <iostream>
#include "point.h"
#include "cercle.h"

using namespace std;

int main()
{
Point P1;
P1.info();
Point P2(1.,1.);
P2.info();
Point P3(P1);
P3.translate(3.,4.);
P3.info();
cout<<"distance P1,P2="<<P2.distance(P1)<<endl;
cout<<"distance P1,P3="<<P3.distance(P1)<<endl;
Cercle C1(P2,3.0);
C1.info();
}
