#ifndef RECTANGLE_H
#define RECTANGLE_H
#include<utility>
#include "point.h"

class Rectangle
{
public:
    Rectangle();
    Rectangle(Point const &s1, Point const &s2);
    Rectangle (float s1x, float s1y, float s2x, float s2y);
    int info () const;
private:
    std::pair<Point,Point> _s;
};

#endif // RECTANGLE_H
