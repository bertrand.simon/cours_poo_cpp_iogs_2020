#ifndef CERCLE_H
#define CERCLE_H
#include "point.h"

class Cercle
{
public:
    Cercle();
    Cercle(Point centre,float rayon);
    Cercle(float cx, float cy,float rayon);
    Cercle(const Cercle &C);
    Point getcentre() const;
    float getrayon() const;
    void setrayon(float r);
    void translate(float vx, float vy);
    void info();

private:
    Point _centre;
    float _rayon;
};

#endif // CERCLE_H
