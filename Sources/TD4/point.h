#ifndef POINT_H
#define POINT_H


class Point
{
public:
    Point();
    Point(float x, float y);
    Point(Point const & P);
    Point& operator= (Point const &P);
    float getx() const;
    float gety() const;
    int  info() const;
    int  translate(float vx, float vy);
    float distance(const Point & P);
private:
    float _x;
    float _y;
};

#endif // POINT_H
