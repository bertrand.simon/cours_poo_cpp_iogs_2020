#include <iostream>
#include "rectangle.h"

Rectangle::Rectangle()
{
Point S1;
Point S2;
_s=std::pair <Point, Point> (S1,S2);
}
Rectangle::Rectangle(Point const & s1, Point const & s2){
    _s=std::pair <Point, Point> (s1,s2);
}
Rectangle::Rectangle(float s1x, float s1y, float s2x, float s2y){
    Point S1(s1x,s1y);
    Point S2 (s2x,s2y);
   _s=std::pair <Point, Point> (S1,S2);
}
int Rectangle::info() const {
    std::cout<<" Est un rectangle avec un sommet ";
    _s.first.info();
    std::cout<<" et un autre sommet ";
    _s.second.info();
    return 0;
}
