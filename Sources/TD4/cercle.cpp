#include <iostream>
#include "cercle.h"

Cercle::Cercle(){
  _centre=Point(0.,0.);
_rayon=1.0;
}
Cercle::Cercle(Point centre,float rayon){
    _centre=centre;
    _rayon=rayon;
}
Cercle::Cercle(float cx, float cy, float rayon){
    _centre=Point(cx,cy);
    _rayon=rayon;
}
Cercle::Cercle(const Cercle &C){
    _centre=C.getcentre();
    _rayon=C.getrayon();
}
Point Cercle::getcentre() const{
    return _centre;
}

float Cercle::getrayon() const{
    return _rayon;
}

void Cercle::setrayon(float r){
    _rayon=r;
}
void Cercle::translate(float vx, float vy){
    _centre.translate(vx,vy);
}

void Cercle::info(){
    std::cout<<"Le cercle a pour centre un ";
    _centre.info();
    std::cout<<"et un rayon de "<<_rayon<<std::endl;
}





